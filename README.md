Nguời viết: Hiển

# Học Python 3 cho devops
## Tổng quan
* Tài liệu xem nhanh để helloworld python code trong khoảng 2 ngày. Dùng cho system admin chưa biết python.
* Python dành cho devops chỉ cần đáp ứng 3 mục tiếu sau là đủ:
  * Biến, kiểu dự liệu, hàm, mảng ( khai báo, và sử dụng biến, hàm, array, dictionary, turtle, list)
  * Xử lý chuỗi, file, folder, chuỗi trong file
  * Thực thi shell scrip(thực thi lệnh, rút trích dữ liệu hệ thống) 
* [Tài liệu này tham khảo từ đây] (https://www.tutorialspoint.com/python3/index.htm)

## Tìm hiểu
### Kiểm tra python version trên Linux
```
root@w1:~# python -V
Python 2.7.12+
```
Có thể trên máy tính có cả 2 version python, 
```
root@w1:~# whereis python
python: /usr/bin/python3.5 /usr/bin/python /usr/bin/python2.7 /usr/bin/python3.5m /usr/lib/python3.5 /usr/lib/python2.7 /usr/lib/python3.6 /etc/python3.5 /etc/python /etc/python2.7 /usr/local/lib/python3.5 /usr/local/lib/python2.7 /usr/share/python /usr/share/man/man1/python.1.gz
```

```
root@w1:~# ls -la /usr/bin/python
lrwxrwxrwx 1 root root 9 Jun  3  2016 /usr/bin/python -> python2.7
```
đổi sang dùng python 3:
```
root@ubuntu:~# rm  /usr/bin/python
root@ubuntu:~# ln -s /usr/bin/python3.5 /usr/bin/python
```

### Đây là ví dụ mẫu:
      
Hàm replace tất cả chuỗi trong file, tạo ra một file mới đã được replace với tên filename.new

```
#Python 3.4.3
filename = "http"
src_string = "/var/www/error/"
des_string = "/var/www/error02/"

def replace_string(filename,src_string,des_string):
    with open(filename, "rt") as inputfile:
        with open(filename + ".new", "wt") as outputfile:
            for line in inputfile:
                outputfile.write(line.replace(src_string,des_string))

replace_string(filename,src_string,des_string)
```
Tìm chuỗi str trong filename, ví dụ này cũng mô tả cách khai báo biến chuỗi filename, định nghĩa 1 hàm, cách nạp input từ bàn phím
```
#Python 3.4.3

filename = "http"
src_string = "/var/www/error/"

def find_string(filename,src_string):
    with open (filename, "rt") as inputfile:
        for line in inputfile:
            if src_string in line:
                print (line)
        return    

filename = input("what file you wanna search for:")
src_string = input("nhap chuoi can thay: ")
print  ("I found:"), find_string(filename,src_string)
```

### Biến và kiểu dữ liệu
* Biến được khai báo như sau:
```
[tên biến] = [giá trị]
# ví dụ
a = 111

b = 11.1

c = "một trắm mười một"

d = [111, 11, 1]

```
* kiểm tra kiểu dữ liệu của biến
```
type(a) 
=> int
type(b) 
=> float
type(d) 
=> array
type(c) 
=> string

```
* Xem các dòng khai báo và gán giá trị cho biến trong ví dụ trên: 19, 20, 21, 49, 50 
* khai báo hàm
```
# cú Pháp:
def [tên hàm]([danh sách biến]):
    xử lý hàm
Ví dụ hàm sửa chuỗi trong file 
def replace_string_in_file(fileName, strSrc, strDes ):
    with open(fileName, "rt") as fin:
        with open(fileName + ".cp", "wt") as fout:
            for line in fin:
                fout.write(line.replace(strSrc, strDes))
```
* Xử lý mảng
  * [Thư viện tra cứu](https://docs.python.org/2/library/array.html?highlight=array#module-array)
  
```
arr = [1, 2, 3, 4, 5]
# truy xuất
arr[0] => 1
arr[-1] => 5
arr[1:3] => [2,3,4]
arr[1:4:2] => [2,4]
# vòng lặp 
for i in a:
   print i 
```

### Thực thi shell script
Sử dụng module [subprocess](https://docs.python.org/2/library/subprocess.html) để thực hiện các lệnh shell
```
import subprocess
# Ví dụ:
# Liệt kê thư mục:
subprocess.call("ls -la", shell=True)
# Thực thi file scrip với tham số
subprocess.call("./echo.sh param1 param2", shell=True)

```

Sử dụng module commands để lấy output từ shell script. Liệt kê danh sách file lưu vào mảng
```
import commands
import string
script = "ls -la"
cmd = commands.getoutput(script)
arr_file = string.split(cmd, "\n")
for i in arr_file:
    print i
```
